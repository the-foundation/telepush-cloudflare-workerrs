
// SET THE FOLLOWING VARIABLE VIA ENV SETTINGS 
//const AUTH_TOK="mytoken@-12312333@someTgBotToken,f33db33f@-123123123123@anothertgBotToken"

//https://api.telegram.org/botTOKEN/deleteMessage?chat_id=CID&message_id=MID
addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
var msg={};

async function TGremoveMessage(message_id,mydest,mytok) {
  
  console.log("deleting "+message_id+" from "+mydest+" @ "+"https://api.telegram.org/bot"+"TG_TOK"+"/deleteMessage?chat_id="+mydest+"&message_id="+message_id )
  const response = await fetch("https://api.telegram.org/bot"+mytok+"/deleteMessage?chat_id="+mydest+"&message_id="+message_id , {
      method: 'GET'
  })
  realres=(await response.text()).toString();
  tmpres=realres.replace(/(^"|"$)/g, '');
  unescapedres=tmpres.replace(new RegExp('\\"', 'g'),'"');
  console.log(unescapedres);
  console.log(realres);
  //console.log(JSON.parse(unescapedres))
  //return JSON.parse(unescapedres);
  return unescapedres;
}

async function TGsendMessage(message,mydest,mytok,silent=false,no_preview=false,my_parse_mode="markdown") {
  if(my_parse_mode=="MarkdownV2") {
//    message=message.replace(/([-\[\]()~`>#+=|{}.!])/g, "\\$1");
message=message.replace(/([-~`>#+=|{}.!])/g, "\\$1");
  }
  
  if(my_parse_mode=="markdown"||my_parse_mode=="MarkdownV2") {
    var count = (message.match(/_/g) || []).length;
    if(count%2 != 0) {
      // improper markdown
      message=message.replace("_"," ");
    }
    var count = (message.match(/\*/g) || []).length;
    if(count%2 != 0) {
      // improper markdown
      message=message.replace("*","");
    }
  }
  var sendmode="sendMessage"

  if(message.toString().length > 4095) {
    // sending a file since msg too long
    sendmode="sendDocument"
    var sendmsg=message.toString().substring(0, 1000)
    //var sendjson=JSON.stringify({
    //  chat_id: mydest,
    //  caption: sendmsg + "(PREVIEW)",
    //  disable_notification: silent,
    //  parse_mode: my_parse_mode,
    //  document: message.toString()
    //})
    console.log("payload_gen")
    var myHeaders = new Headers();
    myHeaders.append("Cookie", "__cfduid=redacted; __cfruid=metoo");
    var formData = new FormData();
    var blob = new Blob([message.toString()], { type: 'plain/text' });
    //formData.append('contentType', "text/plain");
    formData.append('chat_id', mydest);
    formData.append('caption', sendmsg + " =PREVIEW= ");
    formData.append('mydest', silent);
    formData.append('disable_content_type_detection', true);
    formData.append('parse_mode', my_parse_mode);
    formData.append("document", blob , "message.md");
    //formdata.append("payload_json", "{\"content\": \"test\"}");
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: formData,
      redirect: 'follow'
    };
    console.log("sending to "+mydest+" @ "+'https://api.telegram.org/bot' + "TG_TOK" + '/'+sendmode)
    console.log("Response is serialized to:")
    console.log(await new Response(formData).text())
    const response = await fetch('https://api.telegram.org/bot' + mytok + '/'+sendmode, requestOptions)
//    return new Response(JSON.stringify(await hookResponse.json(), null, 2), {status: 200})
    realres=(await response.text()).toString();
    tmpres=realres.replace(/(^"|"$)/g, '');
    unescapedres=tmpres.replace(new RegExp('\\"', 'g'),'"');
    console.log(unescapedres);
    console.log(realres);
    //console.log(JSON.parse(unescapedres))
    //return JSON.parse(unescapedres);
    return unescapedres;
  } else {
    // regular size < 4096
    var sendmsg=message.toString().substring(0, 4095)
    var sendjson=JSON.stringify({
      chat_id: mydest,
      text: sendmsg,
      disable_notification: silent,
      disable_web_page_preview: no_preview,
      parse_mode: my_parse_mode
    })
    console.log("sending to "+mydest+" @ "+'https://api.telegram.org/bot' + "TG_TOK" + '/'+sendmode)
    console.log(sendjson);
    const response = await fetch('https://api.telegram.org/bot' + mytok + '/'+sendmode , {
        method: 'POST',
        headers: {  'Content-Type': 'application/json' },
        body: JSON.stringify({
            chat_id: mydest,
            text: message.toString().substring(0, 4096),
            disable_notification: silent,
            disable_web_page_preview: no_preview,
            parse_mode: my_parse_mode
        })
    })
    realres=(await response.text()).toString();
    tmpres=realres.replace(/(^"|"$)/g, '');
    unescapedres=tmpres.replace(new RegExp('\\"', 'g'),'"');
    console.log(unescapedres);
    console.log(realres);
    //console.log(JSON.parse(unescapedres))
    //return JSON.parse(unescapedres);
    return unescapedres;
  }


}
 
async function handleRequest(request) {
var response_not_sent=true;

 function addTokFunction(item) {
   const myArr=item.split("@");
   TOKARRAY[myArr[0]]=myArr[1];
   AUTHARRAY[myArr[0]]=myArr[2];
 }
 
 const TMPARRAY=AUTH_TOK.split(",");
 const TOKARRAY={};
 const AUTHARRAY={};
 TMPARRAY.forEach(addTokFunction);
 //if(request.body=="") {
 //  msg={};
 //} else {
 //  if (typeof request.body === 'object'){
 //    msg=request.body
 //  }else if (typeof request.body === 'string'){
 //    msg=JSON.parse(request.body)
 //  } else {
 //    return new Response(JSON.stringify({"status":"error", "text": "NO_JSON_SENT"}),{ status: 409 });
 //  }
 //}

// console.log(msg.text);
//console.log(mytxt);
 const uri = request.url.replace(/^https:\/\/.*?\//gi, "/");
 //console.log(uri);
 if(uri=="/api/messages") {
  return new Response(JSON.stringify({"status":"error", "text": "OLD_ENDPOINT"}),{ status: 404 });

 }
 if(uri.startsWith("/api/messages_delete/")) {

  const searchtok=uri.split("/")[3];
  const delmsgid=uri.split("/")[4];
  if(!(searchtok in TOKARRAY)) {
    return new Response(JSON.stringify({"status":"error", "text": "TOKEN_NOT_FOUND"}),{ status: 401 });
  } else {
    const dest=TOKARRAY[searchtok];
    const TG_TOK=AUTHARRAY[searchtok];
    try{
      console.log("removing");
      const myresp=await TGremoveMessage(delmsgid,dest,TG_TOK);
      console.log("remove_req sent..");
      //myresp=await response;
      console.log(myresp);
      //return new Response(JSON.stringify(myresp));
      tmpobj=JSON.parse(myresp);
      var retobj={};
      var resp_code=409;      
      if("ok" in tmpobj) {
        retobj["ok"]=tmpobj.ok;
        if(retobj["ok"]) { resp_code=202 }
      } else {
        retobj["ok"]=false
      }
      return new Response(JSON.stringify(retobj),{ status: resp_code }); 
    } catch(err){
      console.error(err);
      return new Response(JSON.stringify({"status":"error", "text": "ERR_DURING_REMOVING_MSG"}),{ status: 500 });
    }
  } 
 } // end messages_delete

 // end GET-req


 // start POST-req
 try {msg=await request.json()} catch(err) {
  console.log(err);
  return new Response(JSON.stringify({"status":"error", "text": "NO_JSON_SENT"}),{ status: 422 });
 }
 
 var mytxt="";
 var txtfound=false;
 var besilent=false;
 var parsemode="markdown";
 var sendmode="text";
 var link_previews=true;

 try {            if("text" in msg) {  txtfound=true;mytxt=msg.text;    }
 } catch (error) { console.log(error);   }
myorigin="Telepush_cf"
 try {            if("origin" in msg) { myorigin=msg.origin;    }
} catch (error) { console.log(error);   }
try {            if("options" in msg) { if ("silent" in msg.options ) { if(msg.options.silent) { besilent=true } }  }
} catch (error) { console.log(error);   }
//MarkdownV2 HTML markdown
try {            if("parse_mode" in msg) {  parsemode=msg.parse_mode;    }
} catch (error) { console.log(error);   }
try {            if("type" in msg) {  sendmode=msg.type;    }
} catch (error) { console.log(error);   }

try {            if("options" in msg) { if ("disable_link_previews" in msg.options ) { if(msg.options.disable_link_previews) { link_previews=false } }  }
} catch (error) { console.log(error);   }

if(uri.startsWith("/api/messages/")) {

   const searchtok=uri.split("/")[3];
   if(!(searchtok in TOKARRAY)) {
    return new Response(JSON.stringify({"status":"error", "text": "TOKEN_NOT_FOUND"}),{ status: 401 });
   } else {
     const dest=TOKARRAY[searchtok];
     const TG_TOK=AUTHARRAY[searchtok];

     if(txtfound) {
      //const resp="TOKSET_sending "+mytxt+" to "+dest+"  from "+uri+" to "+"https://api.telegram.org/bot"+TG_TOK+"/sendMessage"; 
      //const messageToSend=JSON.stringify(
      //  {"chat_id":dest, "text": mytxt}
      //);
      //return new Response(JSON.stringify(await messageSend()), {
      //  headers: { 'content-type': 'application/json' },
      //})

      //return new Response(resp);
      try{
        console.log("sending");
        const myresp=await TGsendMessage(myorigin+" wrote: \n\n"+mytxt,dest,TG_TOK,besilent,link_previews)
        //const myresp=await fetch(bot_url,{method:'POST',headers:{"Content-Type":"application/json"},body:messageToSend});
        console.log("sent..");
        //myresp=await response;
        console.log(myresp);
        //return new Response(JSON.stringify(myresp));
        tmpobj=JSON.parse(myresp);
        var retobj={};
        var resp_code=409;      
        if("ok" in tmpobj) {
          retobj["ok"]=tmpobj.ok;
          if(retobj["ok"]) { resp_code=202 }
        } else {
          retobj["ok"]=false
        }
        try { 
          if("result" in tmpobj) {
            if("message_id" in tmpobj.result) {
               retobj["message_id"]=tmpobj.result.message_id;
            }            
          }
        } catch(iderr) {
          console.log(iderr); }
          return new Response(JSON.stringify(retobj),{ status: resp_code }); 
        } catch(err){
        console.error(err);
        return new Response(JSON.stringify({"status":"error", "text": "ERR_DURING_SENDING"}),{ status: 500 });
      }
     } else {
      const resp="NOTXT to "+dest+" msg from "+uri+" to "+"https://api.telegram.org/bot"+TG_TOK+"/sendMessage"; 
      return new Response(resp);
     }
   } // end token found
   response_not_sent=false;
 } 

 if(response_not_sent) {
  return new Response(JSON.stringify({"status":"error", "text": "WRONG_URL"}),{ status: 401 });
}

}
